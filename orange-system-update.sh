has_package() {
    pacman -Qq "${1}" > /dev/null 2>&1
}

db_unlock() {
    local DB_LOCK="$(pacman-conf DBPath)db.lck"

    # Remove the database lock only if it's actually present;  of course,
    # it does smell like a TOCTOU issue, but that doesn't matter anyway
    if [[ -f "${DB_LOCK}" ]]; then
        echo "    Unlocking pacman database..."
        rm -f "${DB_LOCK}"
    fi
}

enable_fstrim() {
    systemctl status fstrim.timer > /dev/null 2>&1

    # Enable the timer only if found and not already enabled
    if [[ $? = 3 ]]; then
        echo "==> Enabling periodic trimming of filesystems..."
        systemctl enable $1 --quiet fstrim.timer

        # Optionally, start the associated service once manually
        if [[ "$1" == "--now" ]]; then
            echo "    Starting initial filesystem trimming in background..."
            systemctl start fstrim.service
        fi
    fi
}

post_install() {
    # Ensure enabled fstrim timer from util-linux package
    enable_fstrim
}

post_upgrade() {
    # Remove Calamares autostart files if Calamares isn't installed
    if ! has_package "calamares"; then
        local HOME_PATH HOME_PATHS
        local DESKTOP_FILE DESKTOP_FILES
        local IFS_DEFAULT CONSOLE_NOTIFIED

        HOME_PATHS=$(cat /etc/passwd | gawk --field-separator ':' '{ if ($3 >= 1000 && $3 < 65534) print "\"" $6 "\"" }')
        if [[ ! -z ${HOME_PATHS} ]]; then
            # Handle spaces in file paths properly
            IFS_DEFAULT="${IFS}"
            IFS=$'\n'

            # There could be many users, so loop through them one by one
            for HOME_PATH in ${HOME_PATHS}; do
                DESKTOP_FILES=$(compgen -G "${HOME_PATH//\"/}/.config/autostart/calamares.desktop")
                if [[ ! -z ${DESKTOP_FILES} ]]; then
                    CONSOLE_NOTIFIED=false
                    for DESKTOP_FILE in ${DESKTOP_FILES}; do
                        if [[ "${CONSOLE_NOTIFIED}" = false ]]; then
                            echo "==> Removing redundant Calamares autostart file..."
                            CONSOLE_NOTIFIED=true
                        fi
                        rm -f "${DESKTOP_FILE}"
                    done
                fi
            done

            # Restore the default output splitting
            IFS="${IFS_DEFAULT}"
        fi
    fi

    # Ensure installed xfce4-screensaver to match changes in the Xfce
    # edition profile, which makes screen locking work on Xfce installs
    if has_package "xfwm4" && \
       has_package "xfce4-panel" && \
       ! has_package "xfce4-screensaver"; then
        echo "==> Installing 'xfce4-screensaver' to enable screen locking..."
        db_unlock
        pacman -S xfce4-screensaver --noconfirm
    fi

    # Ensure enabled fstrim timer from util-linux package, and start
    # the timer if it wasn't found to be already enabled
    enable_fstrim --now

    # Uninstall TLP because it provides no benefits, hogs the CPU, and even causes issues,
    # e.g. kernel panics with SATA drives off a PCI Express SATA card in a RockPro64
    if has_package "tlp"; then
        echo "==> Uninstalling redundant 'tlp' package..."
        db_unlock
        systemctl disable --now --quiet tlp
        pacman -R tlp --noconfirm
    fi

    # initialize the GnuPG keyring from scratch.
   
    /usr/bin/rm -rf /etc/pacman.d/gnupg
    /usr/bin/pacman-key --init
    /usr/bin/pacman-key --populate archlinuxarm orange-os
    
    # Notify the user(s) if they need to update their passwords
    local OUTDATED_USER OUTDATED_USERS
    local MESSAGE DESCRIPTION1 DESCRIPTION2
    local NOTIFY_USERID CONSOLE_ALERTED

    OUTDATED_USERS=$(grep '$1' /etc/shadow 2> /dev/null | cut -d ':' -f 1)
    if [[ ! -z ${OUTDATED_USERS} ]]; then
        MESSAGE='Your password needs to be updated.'
        DESCRIPTION1='Because of a system library update, please run "passwd" utility'
        DESCRIPTION2='to update your password before rebooting.'
        CONSOLE_ALERTED=false

        for OUTDATED_USER in ${OUTDATED_USERS}; do
            if users | grep -q -x "${OUTDATED_USER}"; then
                if [[ "${CONSOLE_ALERTED}" = false ]]; then
                    echo "==> ${MESSAGE}"
                    echo "    ${DESCRIPTION1}"
                    echo "    ${DESCRIPTION2}"
                    CONSOLE_ALERTED=true
                fi

                NOTIFY_USERID=$(id -u ${OUTDATED_USER})
                if [[ -e "/run/user/${NOTIFY_USERID}/bus" ]]; then
                    sudo -u ${OUTDATED_USER} \
                        sh -c "DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/${NOTIFY_USERID}/bus \
                               notify-send -u critical '${MESSAGE}' '${DESCRIPTION1} ${DESCRIPTION2}'"
                fi
            fi
        done
    fi
}
